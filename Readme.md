
To work in Windows, you need to
1) Place this repository in $HOME/vimrc
2) Create a .gvimrc file in $HOME with the contents "source $HOME/vimrc"
3) Enjoy vim!
